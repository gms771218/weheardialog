package com.gms.widget.dialog;

import android.annotation.SuppressLint;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 00_收到提醒與警示視窗_目標達成
 * by gms on 2018.10.31
 */
@SuppressLint("ValidFragment")
public class WalkingDialog_3 extends BaseDialog {

    // 標準值
    private int standard = 0;
    // 目前值
    private int value = 0 ;

    public WalkingDialog_3() {
    }

    public WalkingDialog_3(Runnable btnCallback) {
        super(btnCallback);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_general_3;
    }

    @Override
    protected void initView(View view) {

        view.<ImageView>findViewById(R.id.imgView).setImageResource(R.drawable.gif_good);
        view.<TextView>findViewById(R.id.tvMsg).setText(String.format(getString(R.string.msg_walk_3), standard));
        view.<TextView>findViewById(R.id.tvMsg).setMovementMethod(ScrollingMovementMethod.getInstance());
        view.<Button>findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (btnCallback != null) {
                    btnCallback.run();
                }
            }
        });

    }

    @Override
    public String toString() {
        return "00_收到提醒與警示視窗_目標達成";
    }


    public WalkingDialog_3 setStandard(int standard) {
        this.standard = standard;
        return this ;
    }

    public WalkingDialog_3 setValue(int value) {
        this.value = value;
        return this;
    }

}
