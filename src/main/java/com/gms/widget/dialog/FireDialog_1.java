package com.gms.widget.dialog;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 00_收到提醒與警示視窗_火災
 * by gms on 2018.10.31
 */
@SuppressLint("ValidFragment")
public class FireDialog_1 extends BaseDialog {

    public FireDialog_1() {
    }

    public FireDialog_1(Runnable btnCallback) {
        super(btnCallback);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_fire;
    }

    @Override
    protected void initView(View view) {

        view.<ImageView>findViewById(R.id.imgView).setImageResource(R.drawable.gif_fire);
        view.<TextView>findViewById(R.id.tvMsg).setText(getString(R.string.msg_fire_1));
        view.<TextView>findViewById(R.id.tvMsg).setMovementMethod(ScrollingMovementMethod.getInstance());
        view.<Button>findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (btnCallback != null) {
                    btnCallback.run();
                }
            }
        });

        view.<Button>findViewById(R.id.btn119).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:119"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }

    @Override
    public String toString() {
        return "00_收到提醒與警示視窗_火災";
    }

    private void showEmergencyMsg(TextView textView) {
        String _msg = textView.getText().toString() ;
        SpannableStringBuilder builder = new SpannableStringBuilder(_msg);
        builder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.text_color_2)), _msg.lastIndexOf("為")+1, _msg.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        textView.setText(builder);
    }

}
