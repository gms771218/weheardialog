package com.gms.widget.dialog;

import android.annotation.SuppressLint;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 00_收到提醒與警示視窗_心率_緊急
 * by gms on 2018.10.31
 */
@SuppressLint("ValidFragment")
public class HeartDialog_1 extends BaseDialog {

    // 高於或低於(1或0)
    private String moreOrLess = "高";
    // 標準值
    private int standard = 0;
    // 目前值
    private int value = 0;

    public HeartDialog_1() {
    }

    public HeartDialog_1(Runnable btnCallback) {
        super(btnCallback);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_general_1;
    }

    @Override
    protected void initView(View view) {

        view.<ImageView>findViewById(R.id.imgView).setImageResource(R.drawable.gif_heart);
        view.<TextView>findViewById(R.id.tvMsg).setText(String.format(getString(R.string.msg_heart_1), moreOrLess, standard, value)); //
        view.<TextView>findViewById(R.id.tvMsg).setMovementMethod(ScrollingMovementMethod.getInstance());
        view.<Button>findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (btnCallback != null) {
                    btnCallback.run();
                }
            }
        });

        showEmergencyMsg(view.<TextView>findViewById(R.id.tvMsg));
    }

    private void showEmergencyMsg(TextView textView) {
        String _msg = textView.getText().toString() ;
        SpannableStringBuilder builder = new SpannableStringBuilder(_msg);
        builder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.text_color_2)), _msg.lastIndexOf("為")+1, _msg.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        textView.setText(builder);
    }

    @Override
    public String toString() {
        return "00_收到提醒與警示視窗_心率";
    }

    public HeartDialog_1 setStandard(int standard) {
        this.standard = standard;
        return this;
    }

    public HeartDialog_1 setValue(int value) {
        this.value = value;
        return this;
    }

    // 高於或低於(1或0)
    public HeartDialog_1 setMoreOrLess(int moreOrLess) {
        this.moreOrLess = moreOrLess == 1 ? "高" : "低";
        return this;
    }

}
