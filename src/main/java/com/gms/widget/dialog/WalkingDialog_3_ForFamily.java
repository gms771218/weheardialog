package com.gms.widget.dialog;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.content.res.ResourcesCompat;
import com.gms.widget.dialog.util.PermissionUtil;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;

import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

/**
 * 00_收到提醒與警示視窗_目標達成_家人
 * by gms on 2019.01.01
 */
@SuppressLint("ValidFragment")
public class WalkingDialog_3_ForFamily extends BaseDialog implements Observer {

    String name = "";
    String desc = "";
    String head = "" ;

    String phone = "";
    String smsSubject = "步行警告";
    String smsContent = "";

    // 高於或低於(1或0)
    private String moreOrLess = "高";
    // 標準值
    private int standard = 0;
    // 目前值
    private int value = 0;

    public WalkingDialog_3_ForFamily() {
    }

    public WalkingDialog_3_ForFamily(Runnable btnCallback) {
        super(btnCallback);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_walk_family;
    }

    @Override
    protected void initView(View view) {

        view.<ImageView>findViewById(R.id.imgView).setImageResource(R.drawable.gif_walk);
        view.<TextView>findViewById(R.id.tvMsg).setText(String.format(getString(R.string.msg_walk_3_family), name , moreOrLess, standard, value));
        view.<TextView>findViewById(R.id.tvMsgDesc).setText(
                String.format(getString(moreOrLess.equals("高") ? R.string.msg_walk_3_desc_high : R.string.msg_walk_3_desc_low), head, name, value)
        );
        view.<Button>findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (btnCallback != null) {
                    btnCallback.run();
                }
            }
        });

        view.<ImageButton>findViewById(R.id.btnPhone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PermissionUtil(getActivity(), WalkingDialog_3_ForFamily.this).checkCallPhonePermission();
            }
        });

        view.<ImageButton>findViewById(R.id.btnMessage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(String.format("smsto:%s", phone));
                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                it.putExtra("subject", smsSubject);
                it.putExtra("sms_body", smsContent);
                it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it);
            }
        });

    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof PermissionGrantedResponse) {
            // 同意
            // 直接撥號 Intent.ACTION_CALL | 非直接撥號 Intent.ACTION_DIAL
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(String.format("tel:%s", phone)));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (arg instanceof PermissionDeniedResponse) {
            // 拒絕
            //android.widget.Toast.makeText(getContext(), "拒絕電話權限", android.widget.Toast.LENGTH_SHORT).show();
            try {
                final Snackbar snackbar = Snackbar.make(
                        Objects.requireNonNull(getView()),
                        "拒絕電話權限",
                        Snackbar.LENGTH_INDEFINITE
                );
                snackbar.setActionTextColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));
                snackbar.setAction("知道了", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "00_收到提醒與警示視窗_目標達成";
    }


    public WalkingDialog_3_ForFamily setName(String name) {
        this.name = name;
        return this;
    }

    public WalkingDialog_3_ForFamily setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public WalkingDialog_3_ForFamily setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public WalkingDialog_3_ForFamily setStandard(int standard) {
        this.standard = standard;
        return this;
    }

    public WalkingDialog_3_ForFamily setValue(int value) {
        this.value = value;
        return this;
    }

    // 高於或低於(1或0)
    public WalkingDialog_3_ForFamily setMoreOrLess(int moreOrLess) {
        this.moreOrLess = moreOrLess == 1 ? "高" : "低";
        return this;
    }

    // 文字訊息的頭。 ex:10:00了，
    public WalkingDialog_3_ForFamily setHead(String head) {
        this.head = head ;
        return this;
    }
}
