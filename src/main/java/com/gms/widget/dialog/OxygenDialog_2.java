package com.gms.widget.dialog;


import android.annotation.SuppressLint;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 00_收到提醒與警示視窗_血氧_警告
 * by gms on 2018.10.31
 */
@SuppressLint("ValidFragment")
public class OxygenDialog_2 extends BaseDialog {

    // 高於或低於(1或0)
    private String moreOrLess = "高";
    // 標準值
    private int standard = 0;
    // 目前值
    private int value = 0 ;

    public OxygenDialog_2() {
    }

    public OxygenDialog_2(Runnable btnCallback) {
        super(btnCallback);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_general_2;
    }

    @Override
    protected void initView(View view) {

        view.<ImageView>findViewById(R.id.imgView).setImageResource(R.drawable.gif_blood);
        view.<TextView>findViewById(R.id.tvMsg).setText(String.format(getString(R.string.msg_oxygen_2) , moreOrLess, standard, value));
        view.<TextView>findViewById(R.id.tvMsg).setMovementMethod(ScrollingMovementMethod.getInstance());
        view.<Button>findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (btnCallback != null) {
                    btnCallback.run();
                }
            }
        });
    }

    @Override
    public String toString() {
        return "00_收到提醒與警示視窗_血氧_警告";
    }

    private void aa(TextView textView) {
//        SpannableStringBuilder builder = new SpannableStringBuilder(getResources().getString(R.string.msg_oxygen_1));
//        builder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.text_color_2)), 5, 8, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
//        textView.setText(builder);

    }

    public OxygenDialog_2 setStandard(int standard) {
        this.standard = standard;
        return this ;
    }

    public OxygenDialog_2 setValue(int value) {
        this.value = value;
        return this;
    }

    // 高於或低於(1或0)
    public OxygenDialog_2 setMoreOrLess(int moreOrLess) {
        this.moreOrLess = moreOrLess == 1 ? "高" : "低";
        return this;
    }

}
