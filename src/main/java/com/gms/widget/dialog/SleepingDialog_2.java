package com.gms.widget.dialog;

import android.annotation.SuppressLint;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 00_收到提醒與警示視窗_睡眠通知
 * by gms on 2018.10.31
 */
@SuppressLint("ValidFragment")
public class SleepingDialog_2 extends BaseDialog {

    // 高於或低於(1或0)
    private int moreOrLess = 0;
    // 標準值
    private int standard = 0;
    // 目前值
    private int value = 0;

    public SleepingDialog_2() {
    }

    public SleepingDialog_2(Runnable btnCallback) {
        super(btnCallback);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_general_2;
    }

    @Override
    protected void initView(View view) {

        String msg = moreOrLess == 1 ? getString(R.string.msg_sleep_light) : getString(R.string.msg_sleep_deep);

        view.<ImageView>findViewById(R.id.imgView).setImageResource(R.drawable.gif_sleep);
        view.<TextView>findViewById(R.id.tvMsg).setText(String.format(msg, standard, minuteToHM(value)));
        view.<TextView>findViewById(R.id.tvMsg).setMovementMethod(ScrollingMovementMethod.getInstance());
        view.<Button>findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (btnCallback != null) {
                    btnCallback.run();
                }
            }
        });
    }

    @Override
    public String toString() {
        return "00_收到提醒與警示視窗_睡眠通知";
    }

    public SleepingDialog_2 setStandard(int standard) {
        this.standard = standard;
        return this;
    }


    public SleepingDialog_2 setValue(int value) {
        this.value = value;
        return this;
    }

    public SleepingDialog_2 setMoreOrLess(int moreOrLess) {
        this.moreOrLess = moreOrLess;
        return this;
    }

    /**
     * value 分鐘轉換成時分
     */
    private String minuteToHM(int value) {

        String msg = "";
        int minute = value % 60;
        int hour = value / 60;
        if (hour > 0)
            msg += hour + "小時";
        if (minute > 0)
            msg += minute + "分鐘";
        return msg;
    }
}

/*

{"more_or_less": 0, "str_hex": 5c25b8640c1c1, "value": 60, "google.c.a.e": 1, "nts_id": 13, "goal": 0, "user_phone": 0912983242, "head": , "body": 昨晚深睡低於1小時昨晚深睡1小時0分鐘, "standard": 1, "gcm.message_id": 0:1545975921091952%8092433480924334, "body_self": 昨晚深睡低於1小時昨晚深睡1小時0分鐘, "pass": 1, "content": 昨晚的睡眠品質並不好，您是否要聯絡他，關心一下狀況。, "name": shen, "user_id": 221}

 */