package com.gms.widget.dialog.util;

import android.Manifest;
import android.app.Activity;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.Observable;
import java.util.Observer;

public class PermissionUtil extends Observable {

    Activity mActivity;

    public PermissionUtil(Activity activity, Observer observer) {
        this.mActivity = activity;
        this.addObserver(observer);
    }

    protected void checkPermission(String permission) {
        Dexter.withActivity(mActivity)
                .withPermission(permission)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // 同意
                        setChanged();
                        notifyObservers(response);
                        deleteObservers();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // 拒絕
                        setChanged();
                        notifyObservers(response);
                        deleteObservers();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                        deleteObservers();
                    }
                })
                .check();
    }

    public void checkCallPhonePermission() {
        checkPermission(Manifest.permission.CALL_PHONE);
    }


}
