package com.gms.widget.dialog;

import android.annotation.SuppressLint;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 00_收到提醒與警示視窗_呼叫器
 * by gms on 2018.10.31
 */
@SuppressLint("ValidFragment")
public class CallingDialog_2 extends BaseDialog {

    public CallingDialog_2() {
    }

    public CallingDialog_2(Runnable btnCallback) {
        super(btnCallback);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_general_2;
    }

    @Override
    protected void initView(View view) {

        view.<ImageView>findViewById(R.id.imgView).setImageResource(R.drawable.gif_calling);
        view.<TextView>findViewById(R.id.tvMsg).setText(getString(R.string.msg_call_2));
        view.<TextView>findViewById(R.id.tvMsg).setMovementMethod(ScrollingMovementMethod.getInstance());
        view.<Button>findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (btnCallback != null) {
                    btnCallback.run();
                }
            }
        });

    }

    @Override
    public String toString() {
        return "00_收到提醒與警示視窗_呼叫器";
    }
}
