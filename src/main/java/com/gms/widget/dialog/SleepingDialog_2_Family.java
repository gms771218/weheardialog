package com.gms.widget.dialog;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.*;
import androidx.core.content.res.ResourcesCompat;
import com.gms.widget.dialog.util.PermissionUtil;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;

import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

/**
 * 00_收到提醒與警示視窗_睡眠通知_家人
 * by gms on 2019.01.01
 */
@SuppressLint("ValidFragment")
public class SleepingDialog_2_Family extends BaseDialog implements Observer {

    String name = "";
    String desc = "";

    String phone = "";
    String smsSubject = "睡眠警告";
    String smsContent = "";

    // 高於或低於(1或0)
    private int moreOrLess = 0;
    // 標準值
    private int standard = 0;
    // 目前值
    private int value = 0;

    public SleepingDialog_2_Family() {
    }

    public SleepingDialog_2_Family(Runnable btnCallback) {
        super(btnCallback);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_walk_family;
    }

    @Override
    protected void initView(View view) {

        String msg = moreOrLess == 1 ? getString(R.string.msg_sleep_light_family) : getString(R.string.msg_sleep_deep_family);

        view.<ImageView>findViewById(R.id.imgView).setImageResource(R.drawable.gif_sleep);
        view.<TextView>findViewById(R.id.tvMsg).setText(String.format(msg, name , standard, minuteToHM(value)));
        view.<TextView>findViewById(R.id.tvMsgDesc).setText(String.format(getString(R.string.msg_sleep_desc), name));
        view.<Button>findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (btnCallback != null) {
                    btnCallback.run();
                }
            }
        });

        view.<ImageButton>findViewById(R.id.btnPhone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PermissionUtil(getActivity(), SleepingDialog_2_Family.this).checkCallPhonePermission();
            }
        });

        view.<ImageButton>findViewById(R.id.btnMessage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(String.format("smsto:%s", phone));
                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                it.putExtra("subject", smsSubject);
                it.putExtra("sms_body", smsContent);
                it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it);
            }
        });

    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof PermissionGrantedResponse) {
            // 同意
            // 直接撥號 Intent.ACTION_CALL | 非直接撥號 Intent.ACTION_DIAL
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(String.format("tel:%s", phone)));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (arg instanceof PermissionDeniedResponse) {
            // 拒絕
            //android.widget.Toast.makeText(getContext(), "拒絕電話權限", android.widget.Toast.LENGTH_SHORT).show();
            try {
                final Snackbar snackbar = Snackbar.make(
                        Objects.requireNonNull(getView()),
                        "拒絕電話權限",
                        Snackbar.LENGTH_INDEFINITE
                );
                snackbar.setActionTextColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));
                snackbar.setAction("知道了", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "00_收到提醒與警示視窗_目標達成";
    }


    public SleepingDialog_2_Family setName(String name) {
        this.name = name;
        return this;
    }

    public SleepingDialog_2_Family setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public SleepingDialog_2_Family setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public SleepingDialog_2_Family setStandard(int standard) {
        this.standard = standard;
        return this;
    }

    public SleepingDialog_2_Family setValue(int value) {
        this.value = value;
        return this;
    }

    // 高於或低於(1或0)
    public SleepingDialog_2_Family setMoreOrLess(int moreOrLess) {
        this.moreOrLess = moreOrLess;
        return this;
    }

    /**
     * value 分鐘轉換成時分
     */
    private String minuteToHM(int value) {

        String msg = "";
        int minute = value % 60;
        int hour = value / 60;
        if (hour > 0)
            msg += hour + "小時";
        if (minute > 0)
            msg += minute + "分鐘";
        return msg;
    }
}
