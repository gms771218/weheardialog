package com.gms.widget.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public abstract class BaseDialog extends DialogFragment {


    /**
     * Layout id
     * @return
     */
    protected abstract int getLayoutId();

    /**
     * init view
     * @param view
     */
    protected abstract void initView(View view);

    protected Runnable btnCallback ;

    protected String content = "";

    public BaseDialog() {
    }
    public BaseDialog(Runnable btnCallback) {
        this.btnCallback = btnCallback ;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        try {
            LocalBroadcastManager
                    .getInstance(requireContext())
                    .sendBroadcast(new Intent("tw.azul.wehear.action_calm_alarm"));
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container);
        initView(view);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }
    }

    public BaseDialog setContent(String content) {
        this.content = content;
        return this ;
    }
}
