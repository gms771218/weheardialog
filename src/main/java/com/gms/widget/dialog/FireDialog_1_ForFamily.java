package com.gms.widget.dialog;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.content.res.ResourcesCompat;
import com.bumptech.glide.Glide;
import com.gms.widget.dialog.util.PermissionUtil;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;

import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

/**
 * 00_收到提醒與警示視窗_火災_家人
 * by gms on 2018.10.31
 */
@SuppressLint("ValidFragment")
public class FireDialog_1_ForFamily extends BaseDialog implements Observer{

    String name = "";
    String address = "";
    // 地圖連結
    String mapLink = "" ;
    // 地圖圖片
    String mapImg = "https://maps.googleapis.com/maps/api/staticmap?size=580x180&zoom=16&scale=1&key=AIzaSyCde7cxKONfy-4R43FcFl_7BcU3eLhKo0s&markers=25.042073%2C121.532084&center+=25.042073%2C121.532084" ;

    String phone = "";
    String smsSubject = "火災警告";
    String smsContent = "";

    public FireDialog_1_ForFamily() {
    }

    public FireDialog_1_ForFamily(Runnable btnCallback) {
        super(btnCallback);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_fire_family;
    }

    @Override
    protected void initView(View view) {

        view.<ImageView>findViewById(R.id.imgView).setImageResource(R.drawable.gif_fire);
        view.<TextView>findViewById(R.id.tvMsg).setText(String.format(getResources().getString(R.string.msg_fire_1_family_home) , name)); // String.format(getString(R.string.msg_fire_1_family_home), name)
        view.<TextView>findViewById(R.id.tvAddress).setText(address);

        view.<Button>findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (btnCallback != null) {
                    btnCallback.run();
                }
            }
        });

        view.<Button>findViewById(R.id.btn119).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:119"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        view.<ImageButton>findViewById(R.id.btnPhone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PermissionUtil(getActivity(), FireDialog_1_ForFamily.this ).checkCallPhonePermission();
            }
        });

        view.<ImageButton>findViewById(R.id.btnMessage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(String.format("smsto:%s", phone));
                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                it.putExtra("subject", smsSubject);
                it.putExtra("sms_body", smsContent);
                it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it);
            }
        });


        view.<ImageView>findViewById(R.id.imgMap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mapLink.isEmpty())
                    return;
                Uri uri = Uri.parse(mapLink);
                String address = uri.getQueryParameter("query") ;

                String map = "http://maps.google.co.in/maps?q=" + address;
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

//                Uri gmmIntentUri = Uri.parse(String.format("geo:%s,%s" ,location[0] , location[1] ));
//                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                mapIntent.setPackage("com.google.android.apps.maps");
//                startActivity(mapIntent);
            }
        });

        Glide.with(getActivity())
                .load(mapImg)
                .into(view.<ImageView>findViewById(R.id.imgMap));

    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof PermissionGrantedResponse) {
            // 同意
            // 直接撥號 Intent.ACTION_CALL | 非直接撥號 Intent.ACTION_DIAL
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(String.format("tel:%s" , phone )));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (arg instanceof PermissionDeniedResponse) {
            // 拒絕
            //android.widget.Toast.makeText(getContext(), "拒絕電話權限", android.widget.Toast.LENGTH_SHORT).show();
            try {
                final Snackbar snackbar = Snackbar.make(
                        Objects.requireNonNull(getView()),
                        "拒絕電話權限",
                        Snackbar.LENGTH_INDEFINITE
                );
                snackbar.setActionTextColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));
                snackbar.setAction("知道了", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "00_收到提醒與警示視窗_火災_家人";
    }


    public FireDialog_1_ForFamily setName(String name) {
        this.name = name;
        return this;
    }

    public FireDialog_1_ForFamily setAddress(String address) {
        this.address = address;
        return this;
    }

    public FireDialog_1_ForFamily setMapLink(String mapLink) {
        this.mapLink = mapLink;
        return this ;
    }

    public FireDialog_1_ForFamily setMapImg(String mapImg) {
        this.mapImg = mapImg;
        return this ;
    }

    public FireDialog_1_ForFamily setPhone(String phone) {
        this.phone = phone;
        return this;
    }
}
