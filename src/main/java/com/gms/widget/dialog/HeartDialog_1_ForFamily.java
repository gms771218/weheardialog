package com.gms.widget.dialog;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.content.res.ResourcesCompat;
import com.gms.widget.dialog.util.PermissionUtil;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;

import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

/**
 * 00_收到提醒與警示視窗_心率_家人_緊急
 * by gms on 2019.01.02
 */
@SuppressLint("ValidFragment")
public class HeartDialog_1_ForFamily extends BaseDialog implements Observer {

    String name = "" ;
    String desc = "" ;

    String phone = "";
    String smsSubject = "心率警告";
    String smsContent = "";

    // 高於或低於(1或0)
    private String moreOrLess = "高";
    // 標準值
    private int standard = 0;
    // 目前值
    private int value = 0 ;

    public HeartDialog_1_ForFamily() {
    }

    public HeartDialog_1_ForFamily(Runnable btnCallback) {
        super(btnCallback);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_oxygen_family;
    }

    @Override
    protected void initView(View view) {

        view.<ImageView>findViewById(R.id.imgView).setImageResource(R.drawable.gif_heart);
        view.<TextView>findViewById(R.id.tvMsg).setText(String.format(getString(R.string.msg_heart_1_family) , name , moreOrLess , standard , value));
        view.<TextView>findViewById(R.id.tvMsgDesc).setText(String.format(getString(R.string.msg_heart_1_desc) , name , moreOrLess , standard));
        view.<Button>findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (btnCallback != null) {
                    btnCallback.run();
                }
            }
        });

        view.<ImageButton>findViewById(R.id.btnPhone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PermissionUtil(getActivity(), HeartDialog_1_ForFamily.this ).checkCallPhonePermission();
            }
        });

        view.<ImageButton>findViewById(R.id.btnMessage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(String.format("smsto:%s", phone));
                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                it.putExtra("subject", smsSubject);
                it.putExtra("sms_body", smsContent);
                it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it);
            }
        });

        showEmergencyMsg(view.<TextView>findViewById(R.id.tvMsg));
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof PermissionGrantedResponse) {
            // 同意
            // 直接撥號 Intent.ACTION_CALL | 非直接撥號 Intent.ACTION_DIAL
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(String.format("tel:%s" , phone )));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (arg instanceof PermissionDeniedResponse) {
            // 拒絕
            //android.widget.Toast.makeText(getContext(), "拒絕電話權限", android.widget.Toast.LENGTH_SHORT).show();
            try {
                final Snackbar snackbar = Snackbar.make(
                        Objects.requireNonNull(getView()),
                        "拒絕電話權限",
                        Snackbar.LENGTH_INDEFINITE
                );
                snackbar.setActionTextColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));
                snackbar.setAction("知道了", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "00_收到提醒與警示視窗_心率_家人";
    }

    private void showEmergencyMsg(TextView textView) {
        String _msg = textView.getText().toString() ;
        SpannableStringBuilder builder = new SpannableStringBuilder(_msg);
        builder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.text_color_2)), _msg.lastIndexOf("為")+1, _msg.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        textView.setText(builder);
    }

    public HeartDialog_1_ForFamily setName(String name) {
        this.name = name;
        return this ;
    }

    public HeartDialog_1_ForFamily setDesc(String desc) {
        this.desc = desc;
        return this ;
    }

    public HeartDialog_1_ForFamily setPhone(String phone) {
        this.phone = phone;
        return this ;
    }

    public HeartDialog_1_ForFamily setStandard(int standard) {
        this.standard = standard;
        return this ;
    }

    public HeartDialog_1_ForFamily setValue(int value) {
        this.value = value;
        return this;
    }
    // 高於或低於(1或0)
    public HeartDialog_1_ForFamily setMoreOrLess(int moreOrLess) {
        this.moreOrLess = moreOrLess == 1 ? "高" : "低";
        return this;
    }
}
